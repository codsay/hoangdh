Hòn đá cô đơn
--------------------------------------------------------------------

[C]Có hòn đá cô đơn [G]xa xa
[Am]Đứng ở đó cớ sao một [Em]mình
[F]Phải chăng đá cũng [C]thất tình
[G]Hoà niềm đau với [C]ta.

[C]Có chị gió bay [G]ngang qua
[Am]Khẽ nhẹ vuốt mát tâm hồn [Em]mình
[F]Này cậu trai [C]thất tình
[G]Buồn làm chi hỡi [C]em?

[C]Có giọt nước rơi [G]trên mi
[Am]Khẽ nhẹ thấm xót xa trong [Em]lòng
[F]Hình như nước cũng [C]biết rằng
[G]Nàng đã xa cách [C]ta.

[C]Ôi giọt nước đau thương [G7]kia ơi
[Am]Chớ vội khóc khiến ta thêm [Em]buồn
[F]Vì ta cũng đã [C]biết rằng
[G]Nàng đã xa cách [C]ta.

[C]Khi người nỡ quay lưng [G]ra đi
[Am]Dẫu còn chút vấn vương trong [Em]lòng
[F]Thì người yêu ơi ta [C]biết rằng
[G]Nàng đã xa cách [C]ta.

[C]Ta ôm một chút đau [G]thương thôi
[Am]Giấu thật kín mãi trong tâm [Em]hồn
[F]Vì người yêu ơi ta [C]biết rằng
[G]Nàng sẽ quên mất [C]ta.